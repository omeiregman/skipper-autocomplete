### 1. What is the difference between Component and PureComponent?
The main different between Component and Pure Component is how they handle shouldComponentUpdate, Pure Components uses a shallow prop and state comparison, meaning there would be no re-render if state and props update with same values. 
On the other hand, React Components re-renders by default on every state or prop update.

Example where it might break:
Pure components would not catch state changes that have deep object mutability (ex. If an objects property changes but the reference still remains the same)


### 2. Context + ShouldComponentUpdate might be dangerous. Why is that?
This could lead to component not re-rendering and a stale display as using shouldComponentUpdate in a component does not affect updates from context changes.

### 3. Describe 3 ways to pass information from a component to its PARENT.
- Callback functions: Parent components can pass a function as a prop to the child, and in return the child calls the function to pass the data back.
- State management libraries: External Libraries such as redux can be used to manage and pass state not only between direct parent and child component, but also between deeply nested child components.
- React context can be used and would have more advantage for more complex component trees as it is used to manage state in a manner both parent and children can interact with directly. 

### 4. Give 2 ways to prevent components from re-rendering.
- React.memo can be used to memoise the render output based on props. 
- Using keys to avoid re-renders, when rendering a list of items, if each list element has a consistent key, React can avoid re-rendering components even when list items are added or removed.

###  5. What is a fragment and why do we need it?
A React Fragment allows us to wrap multiple elements returned from a component without introducing a DOM element like <div>, 
Fragments are useful as it prevents us from adding extra nodes to the DOM.

Example where it might break the app:
When relying on CSS child selectors that target children, a fragment could cause the styles not to apply as it does not add a DOM node.

### 6. Give 3 examples of the HOC pattern.
-  `React.memo` is a higher order component used to memoize a functional component allowing  to only re-render the component if the props have changed.

```tsx
const MyComponent = React.memo(function MyComponent(props) {
  return <div>{props}</div>;
});
```
- connect (redux): is an HOC from Redux that connects a React component to the Redux store.
- withRouter (React Router): is a HOC from react-router that is useful for accessing router properties without prop drilling or using hooks.

###  7. What's the difference in handling exceptions in promises, callbacks, and async...await?
The major difference while handling exceptions in promises, callbacks and async/await is
- Promises Use `.catch()` to handle errors
- Callbacks handle errors as the first argument
- Async/await, we generally use try/catch blocks to handle errors

```javascript
// Promises
fetchData().then(response => console.log(response)).catch(error => console.error(error));

// Callbacks
fetchData((error, response) => {
  if (error) console.error(error);
  else console.log(response);
});

// Async/await
async function fetchData() {
    try {
        const response = await fetchData();
        console.log(response);
    } catch (error) {
        console.error(error);
    }
}

```

### 8. How many arguments does setState take and why is it async?
`setState` in React component can take up to two arguments.
`setState` is asynchronous because it optimizes updates for performance, batching them and updating the DOM in a single pass.

### 9. List the steps needed to migrate a Class to Function Component.
- Convert to a function removing the class syntax from the component definition.
- Utilize useEffect for life cycle methods. Replace lifecycle methods like componentDidMount, componentDidUpdate, and componentWillUnmount with useEffect.
- Replace this.state and this.setState with useState.
- Replace contextType or Consumer with useContext.
- Handle refs with useRef if applicable.

### 10. List a few ways styles can be used with components.
- Inline styling
- CSS Modules
- Styled Components
- CSS-in-JS libraries (Emotion or JSS)

### 11. How to render an HTML string coming from the server.
To render an HTML string from the server, we can make use of `dangerouslySetInnerHTML` attribute. Note, this should be used cautiously to avoid XSS (Cross-Site Scripting) vulnerabilities.
