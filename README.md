# Deel Autocomplete -

## Overview

This project implements an autocomplete component in React using TypeScript. 
The component provides suggestion features as the user types, enhancing user experience by allowing quick and error-free input.


## Features

- **No External Dependencies**: Built with pure React, no third-party state management or UI libraries are used.
- **Dynamic Suggestions**: Updates suggestions based on user input.
- **Debounced Input**: Reduces the frequency of API calls by debouncing the input, enhancing performance.
- **Error Handling**: Gracefully handles errors such as failed data fetching.
- **Highlight Matching Text**: Highlights the part of the suggestions that matches the user input.
- **Keyboard Navigation**: Users can navigate through the suggestions using the Arrow keys (`ArrowUp` and `ArrowDown`). Press `Enter` to select a suggestion, and `Escape` to close the suggestion list.
- **Mouse Support**: Suggestions can be clicked to select, and mouse over events change the highlight in the suggestion list, improving usability and accessibility.
- **Accessibility**: Implements ARIA roles and properties to ensure the component is accessible.
- **Preview**: The project can be previewed on GitLab pages *https://omeiregman.gitlab.io/deel-autocomplete/*


## Installation
To get started with this project, clone the repository and install the dependencies:

### Below you will find some information on how to run the application locally.

#### Install Dependencies
```shell
yarn install
```


#### Run the application locally
```shell
yarn run dev
````


#### Open application in local environment
**http://localhost:3000/deel-autocomplete/**


#### Run tests
```shell
yarn run test
````


#### Build
```shell
yarn run build
````

