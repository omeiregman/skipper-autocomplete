import React, { KeyboardEvent, useCallback, useEffect, useState } from "react";
import { useDebounce } from "../../hooks/useDebounce.ts";
import InputField from "../InputField/InputField.tsx";
import SuggestionList from "../SuggestionList/SuggestionList.tsx";
import { filterSuggestions } from "../../utils/utils.ts";
import { InteractionMode, Suggestion } from "../../types.ts";
import style from "./Autocomplete.module.css";

const DEBOUNCE_INTERVAL = 200;

const Autocomplete: React.FC = () => {
  const [input, setInput] = useState<string>("");
  const [suggestions, setSuggestions] = useState<Suggestion[]>([]);
  const [highlightedIndex, setHighlightedIndex] = useState<number>(-1);
  const [isSuggestionSelected, setIsSuggestionSelected] =
    useState<boolean>(false);
  const [noMatches, setNoMatches] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const [interactionMode, setInteractionMode] = useState<InteractionMode>(
    InteractionMode.KEYBOARD
  );
  const debouncedInput = useDebounce(input, DEBOUNCE_INTERVAL);

  useEffect(() => {
    if (debouncedInput && !isSuggestionSelected) {
      filterSuggestions(debouncedInput)
        .then((filteredSuggestions) => {
          setSuggestions(filteredSuggestions);
          setNoMatches(filteredSuggestions.length === 0);
          setHighlightedIndex(-1);
        })
        .catch((err) => {
          setError("Failed to fetch suggestions. Please try again.");
          console.error(err);
        });
    } else {
      setSuggestions([]);
    }
  }, [debouncedInput, isSuggestionSelected]);

  const handleInputChange = useCallback((newValue: string) => {
    setInput(newValue);
    setIsSuggestionSelected(false);
  }, []);

  const handleSuggestionSelect = useCallback((suggestion: string) => {
    setInput(suggestion);
    setSuggestions([]);
    setHighlightedIndex(-1);
    setIsSuggestionSelected(true);
  }, []);

  const handleKeyDown = useCallback(
    (e: KeyboardEvent<HTMLInputElement>) => {
      if (suggestions.length > 0) {
        setInteractionMode(InteractionMode.KEYBOARD);
        switch (e.key) {
          case "ArrowDown":
            e.preventDefault();
            setHighlightedIndex((prev) =>
              Math.min(prev + 1, suggestions.length - 1)
            );
            break;
          case "ArrowUp":
            e.preventDefault();
            setHighlightedIndex((prev) => Math.max(prev - 1, 0));
            break;
          case "Enter":
            if (highlightedIndex >= 0) {
              handleSuggestionSelect(suggestions[highlightedIndex].name);
            }
            break;
          case "Escape":
            setSuggestions([]);
            break;
        }
      } else {
        setNoMatches(false);
      }
    },
    [highlightedIndex, suggestions, handleSuggestionSelect]
  );

  return (
    <div>
      <InputField
        value={input}
        onChange={handleInputChange}
        onKeyDown={handleKeyDown}
        autoFocus
      />
      {suggestions.length > 0 ? (
        <SuggestionList
          suggestions={suggestions}
          query={debouncedInput}
          onSuggestionSelect={handleSuggestionSelect}
          highlightedIndex={highlightedIndex}
          setHighlightedIndex={setHighlightedIndex}
          interactionMode={interactionMode}
          setInteractionMode={setInteractionMode}
        />
      ) : (
        noMatches && <div className={style.noSuggestions}>No match found</div>
      )}

      {error && <div className={style.error}>{error}</div>}
    </div>
  );
};

export default Autocomplete;
