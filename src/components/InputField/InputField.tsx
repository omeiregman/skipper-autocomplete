import React, { ChangeEvent, KeyboardEvent, useEffect, useRef } from "react";
import style from "./InputField.module.css";
interface InputFieldProps {
  value: string;
  onChange: (value: string) => void;
  onKeyDown: (value: KeyboardEvent<HTMLInputElement>) => void;
  autoFocus?: boolean;
}

const InputField: React.FC<InputFieldProps> = ({
  value,
  onChange,
  onKeyDown,
  autoFocus,
}) => {
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (autoFocus) {
      inputRef.current?.focus();
    }
  }, [autoFocus]);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    onChange(e.target.value);
  };

  return (
    <input
      ref={inputRef}
      type="text"
      className={style.autocompleteInput}
      aria-label="Search"
      aria-autocomplete="list"
      aria-controls="autocomplete-list"
      value={value}
      onChange={handleChange}
      onKeyDown={onKeyDown}
    />
  );
};

export default InputField;
