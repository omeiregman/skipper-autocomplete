import React, { useCallback, useEffect, useRef } from "react";
import { InteractionMode, Suggestion } from "../../types.ts";
import styles from "./SuggestionList.module.css";

interface SuggestionListProps {
  suggestions: Suggestion[];
  query: string;
  onSuggestionSelect: (suggestion: string) => void;
  highlightedIndex: number;
  setHighlightedIndex: (value: number) => void;
  interactionMode: InteractionMode;
  setInteractionMode: (value: InteractionMode) => void;
}

const SuggestionList: React.FC<SuggestionListProps> = ({
  suggestions,
  query,
  onSuggestionSelect,
  highlightedIndex,
  setHighlightedIndex,
  interactionMode,
  setInteractionMode,
}) => {
  const trimmedQuery = query.trim();
  const listContainerRef = useRef<HTMLUListElement>(null);

  const itemRefs = useRef<React.RefObject<HTMLLIElement>[]>([]);
  itemRefs.current = suggestions.map(
    (_, index) => itemRefs.current[index] ?? React.createRef()
  );

  const handleMouseEnter = useCallback(
    (index: number) => {
      if (interactionMode === InteractionMode.MOUSE) {
        setHighlightedIndex(index);
      }
    },
    [interactionMode, setHighlightedIndex]
  );

  const handleMouseMove = useCallback(() => {
    if (interactionMode !== InteractionMode.MOUSE) {
      setInteractionMode(InteractionMode.MOUSE);
    }
  }, [interactionMode, setInteractionMode]);

  useEffect(() => {
    if (highlightedIndex >= 0 && highlightedIndex < itemRefs.current.length) {
      itemRefs?.current[highlightedIndex]?.current?.scrollIntoView({
        behavior: "smooth",
        block: "nearest",
      });
    }
  }, [highlightedIndex]);

  return (
    <ul
      className={styles.suggestionsList}
      id="autocomplete-list"
      role="listbox"
      ref={listContainerRef}
    >
      {suggestions.map((suggestion, index) => {
        const parts = suggestion.name.split(
          new RegExp(`(${trimmedQuery})`, "gi")
        );
        const isHighlighted = index === highlightedIndex;
        const highlightedClass = isHighlighted
          ? styles.suggestionHighlighted
          : "";

        return (
          <li
            ref={itemRefs.current[index]}
            key={suggestion.id}
            role="option"
            id={`suggestion-${index}`}
            className={highlightedClass}
            onClick={() => onSuggestionSelect(suggestion.name)}
            onMouseEnter={() => handleMouseEnter(index)}
            onMouseMove={handleMouseMove}
          >
            {parts.map((part, index) => (
              <span
                key={index}
                className={
                  part.toLowerCase() === trimmedQuery.toLowerCase()
                    ? styles.textBold
                    : ""
                }
              >
                {part}
              </span>
            ))}
          </li>
        );
      })}
    </ul>
  );
};

export default SuggestionList;
