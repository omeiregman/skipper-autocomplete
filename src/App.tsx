import "./App.css";
import Autocomplete from "./components/Autocomplete/Autocomplete.tsx";

function App() {
  return (
    <div>
      <h2 className="title">Deel Autocomplete</h2>
      <p className="sub-text">Search for fruits</p>
      <Autocomplete />
    </div>
  );
}

export default App;
