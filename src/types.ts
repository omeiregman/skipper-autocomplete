export interface Suggestion {
  id: number;
  name: string;
}

export enum InteractionMode {
  MOUSE = "mouse",
  KEYBOARD = "keyboard",
}
