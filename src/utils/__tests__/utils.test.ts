import { describe, it, expect, vi } from "vitest";
import { filterSuggestions } from "../utils.ts";

vi.mock("../mockData", () => ({
  mockData: [
    { id: 1, name: "Apple" },
    { id: 2, name: "Banana" },
    { id: 3, name: "Cherry" },
  ],
}));

describe("filterSuggestions", () => {
  it("should return matching suggestions for valid input", async () => {
    const results = await filterSuggestions("a");
    expect(results).toEqual([
      { id: 1, name: "Apple" },
      { id: 2, name: "Banana" },
    ]);
  });

  it("should reject when input is empty", async () => {
    try {
      await filterSuggestions("");
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
      expect(error).toBe("Input is empty");
    }
  });

  it("should return an empty array when no matches are found", async () => {
    const results = await filterSuggestions("grape");
    expect(results).toEqual([]);
  });
});
