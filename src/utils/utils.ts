import { Suggestion } from "../types.ts";
import { mockData } from "./mockData.ts";

export const filterSuggestions = async (
  input: string
): Promise<Suggestion[]> => {
  try {
    return await new Promise<Suggestion[]>((resolve, reject) => {
      if (!input) reject("Input is empty");
      setTimeout(() => {
        const filtered = mockData.filter((suggestion) =>
          suggestion.name.toLowerCase().includes(input.trim().toLowerCase())
        );
        resolve(filtered);
      }, 250);
    });
  } catch (error) {
    console.error("Failed to fetch suggestions:", error);
    return [];
  }
};
